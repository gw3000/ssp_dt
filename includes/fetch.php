<?php

function is_number($item)
{
    $y=preg_match("^-?\d{0,}[{.,}]\d{0,}", $item);
    if (preg_match("'^-?\d{0,}[{.,}]\d{0,}'", $item)) {
        if (stristr($item, ',')) {
            $item = str_replace(',', '.', $item);
        }
        return $item;
    } elseif (is_numeric($item)) {
        return $item;
    }
}

require_once 'db.php';

$request = $_REQUEST;

$col = array(
    0   =>  'name',
    1   =>  'salary',
    2   =>  'age',
    3   =>  'jobname'
);


$sql = "SELECT name, salary, age, jobname FROM tbperson tp INNER JOIN tbjobs tj ON tp.jobid = tj.id";

// number of rows
$stmt = $pdo->prepare($sql);
$stmt->execute();
$rows_max = $stmt->rowCount();
$rows_filtered = $rows_max;

// filtering
if (!empty($request['search']['value'])) {
    $sql .= " AND (name ilike '%" . $request['search']['value'] . "%' ";
    if (is_number($request['search']['value'])) {
        $sql .= " OR salary = '" . is_number($request['search']['value']) . "' ";
        $sql .= " OR age = '" . is_number($request['search']['value']) . "' ";
    }
    $sql .= " OR jobname Like '%" . $request['search']['value'] . "%' )";
}
$stmt = $pdo->prepare($sql);
$stmt->execute();
$rows_filtered = $stmt->rowCount();

// order
if ($request['length'] != -1) {
    $sql .= " ORDER BY " . $col[$request['order'][0]['column']] . " " . $request['order'][0]['dir'] . " OFFSET " . $request['start'] . " LIMIT " . $request['length'] . "; ";
} else {
    $sql .= " ORDER BY " . $col[$request['order'][0]['column']] . ";";
}


// fetch all results
$stmt = $pdo->prepare($sql);
$stmt->execute();

$data = array();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $subdata = array();
    $subdata[] = $row['name']; //id
    $subdata[] = $row['salary']; //name
    $subdata[] = $row['age']; //salary
    $subdata[] = $row['jobname']; //age
    $data[] = $subdata;
}

$json_data = array(
    "draw"              =>  intval($request['draw']),
    "recordsTotal"      =>  intval($rows_max),
    "recordsFiltered"   =>  intval($rows_filtered),
    "data"              =>  $data
);

echo json_encode($json_data);
